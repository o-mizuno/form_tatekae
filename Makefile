#!/usr/bin/make -f

CP=cp
RM=rm -f
TARG=tatekae
TARGBIB=ses.bib
TEXCMD=platex
BIBTEXCMD=pbibtex
DVIPDFCMD=dvipdfmx

.PHONY: all clean

all:$(TARG).pdf

.SUFFIXES: .pdf .tex .aux .bbl

.tex.pdf:
	@\
	aux=$(<:%.tex=%.aux);\
	$(CP) $$aux $$aux-;\
	$(TEXCMD) $<;\
	echo ========;\
	diff -q $$aux $$aux- > /dev/null;\
	if [ $$? -eq 0 ]; then\
		echo No need for second compile.;\
	else\
		$(BIBTEXCMD) $$aux;\
		echo ========;\
		$(CP) $$aux $$aux-;\
		$(TEXCMD) $<;\
		echo ========;\
		diff -q $$aux $$aux- > /dev/null;\
		if [ $$? -eq 0 ]; then\
			echo No need for third compile.;\
		else\
			$(TEXCMD) $<;\
		fi;\
	fi;\
	$(RM) $$aux-
	$(DVIPDFCMD) $(TARG)

$(TARG).pdf: $(TARG).tex

clean:
	$(RM) $(TARG).{pdf,aux,bbl,log,blg}
